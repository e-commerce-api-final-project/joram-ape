import { Table, Row, Col, Form, InputGroup, Button} from 'react-bootstrap';
import { useParams, Link, useNavigate} from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'


export default function Checkout() {

const {user} = useContext(UserContext);

const {productId} = useParams();
const {quantity} = useParams();
const Navigate = useNavigate();

const [productName, setProductName] = useState("");
const [price, setPrice] = useState("");


const checkOut = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId : productId,
        productName : productName,
        quantity : quantity,
        price : price
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data === true){
        Swal.fire({
          title: "Successfully Enrolled",
          icon: "success",
          text: "You have successfully enrolled for this course."
        })

          setProductName("")
           setPrice("")
        Navigate("/my-account/");
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again"
        })
      }
    })
  }




useEffect(() => {
  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/${quantity}`)
    .then(res => res.json())
    .then(data => {


    setProductName(data.productName)
    setPrice(data.price)
   
    })
},[productId])



  return (

    <Row style={{marginBottom : 200}}>
      <Col xs="12" >
       <h1 className="text-center mt-5">Checkout</h1>
       <hr  />
      </Col>

     {/*Product Form*/}
       <Col xs="8" className="mx-auto">
       <h1>Your Order</h1>

       <Form onSubmit={checkOut}>
        <Table striped bordered hover className="mt-5">
          <thead style={{backgroundColor: "black"}}>
            <tr className="text-white">
              <th>Product</th>
              <th>Quantity</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td >
               {productName}
              </td>
              <td>
                {quantity}
              </td>
              <td >
               {price}
              </td>
            </tr>
          </tbody>
        </Table>

        <Table striped bordered hover className="mt-5">
          <tbody>
             <tr>
              <td><b>Subtotal</b></td>
              <td colSpan={2} className="text-center">
              ₱ {price * quantity}
              </td>
            </tr>
            <tr>
              <td><b>Shipping</b></td>
              <td colSpan={2} className="text-center">
                Free shipping
              </td>
            </tr>
            <tr>
              <td><b>Total Amount</b></td>
              <td colSpan={2} className="text-center">

              ₱ {price * quantity}

              </td>
            </tr>
          </tbody>
        </Table>

        <hr className="mt-5" />
       {['radio'].map((type) => (
        <div key={`inline-${type}`} className="mb-3">
        <Form.Group>
          <Form.Check
            inline
            label="Gcash"
            name="group1"
            type={type}
            id={`inline-${type}-Gcash`}
          />
          </Form.Group>

          <Form.Group>
          <Form.Check
            inline
            label="Palawan Express"
            name="group1"
            type={type}
            id={`inline-${type}-Palawan Express`}
          />
        </Form.Group>
        </div>
      ))}
        {(user.id !== null) ?
               <Button className='mx-2 px-5 mt-4 w-100' variant="dark" onClick={() => checkOut(productId)}>PLACE ORDER</Button>
          :
              <Button className='mx-2 px-5 mt-4 w-100' variant="danger" disabled>Please login</Button>
        }
    </Form>
       </Col>
    </Row>
  );
}



