import { Nav, Button,Row,Col,Card,Form} from 'react-bootstrap';
import { Navigate, NavLink } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

  const { user, setUser } = useContext(UserContext);


  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState("true")

  function authenticate(e) {

    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
    
      headers: {
        'Content-Type': 'application/json'
      },
    
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
  
    .then(res => res.json())
    .then(data => {
   
      console.log(data)

     
      if(typeof data.access !== "undefined"){


        localStorage.setItem('token', data.access);
        retrieveUserDetails(data.access);

        Swal.fire({
          title: 'Login Successful',
          icon: 'success',
          text: 'Welcome to Zuitt!'
        })
           setEmail("");
           setPassword("");

      } else {
        Swal.fire({
          title: "Authentication Failed!",
          icon: 'error',
          text: 'Please, check you login details and try again!'
        })
          setEmail("");
          setPassword("");
      }
    }); 
  }

  const retrieveUserDetails = (token) => {

    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setUser({
        id: data._id,
        isAdmin: data.isAdmin,
        firstName: data.firstName,
        lastName: data.lastName
      });
    });
  };



  useEffect(() => {
    if(email !== "" && password !== ""){
      setIsActive(true)
    }else {
      setIsActive(false)
    }
  }, [email, password]);

  return (
 (user.isAdmin === true) ?  
   <Navigate to="/dashboard" />
   :
   (user.isAdmin === false) ?  
    <Navigate to="/shop-products/" />
   :
     <Row className='pt-5 d-flex justify-content-center align-items-center h-100'>
     <Form onSubmit={(e) => authenticate(e)}>
        <Col>
          <Card className='bg-dark text-white my-5 mx-auto' style={{maxWidth: '600px'}}>
            <Card.Body className='p-5 d-flex flex-column align-items-center'>
              <h2 className="fw-bold mb-3 text-uppercase">Login</h2>
              <p className="text-white-50 mb-5">Please enter your Email and password!</p>
                <Form.Group controlId="userEmail"  className='mx-2 px-5 mt-4 w-100'>
                  <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email here"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                     </Form.Group>
                     <Form.Group controlId="password" className='mx-2 px-5 mt-4 w-100'>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          type="password"
                          placeholder="Password"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                          required
                        />
                      </Form.Group>
                     { isActive ? 
                        <Button className='mx-2 px-5 mt-4 w-100' variant="outline-secondary" size="md" type="submit" id="submitBtn">Login</Button>
                        : 
                        <Button className='mx-2 px-5 mt-4 w-100' variant="danger my-3"  size="md" type="submit" id="submitBtn" disabled>Submit</Button>
                      }
                      <p className="mt-5">Don't have an account?</p>
                <Nav.Item>
                    <Nav.Link class="text-white-50 fw-bold" as={NavLink} to="/Register">Sign Up</Nav.Link>
                </Nav.Item>
            </Card.Body>
          </Card>
        </Col>
      </Form>
    </Row>
    
  );
}

